# -*- coding: utf-8 -*-
# vim: ft=yaml

{% from "swall45/map.jinja" import swall45 with context %}

include:
  - swall45.packages_service
  - swall45.interfaces_zones
  - swall45.param_rule

