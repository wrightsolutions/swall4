# -*- coding: utf-8 -*-
# vim: ft=yaml

{% from "swall45/map.jinja" import swall45 with context %}

include:
  - swall45

interfaces4:
  file.managed:
    - name: /etc/shorewall/interfaces
    - source: salt://swall45/files/interfaces.j2
    - template: jinja
    - mode: 640
    - user: root
    - group: root

zones4:
  file.managed:
    - name: /etc/shorewall/zones
    - source: salt://swall45/files/zones.j2
    - template: jinja
    - mode: 640
    - user: root
    - group: root

