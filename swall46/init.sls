# -*- coding: utf-8 -*-
# vim: ft=yaml

{% from "swall46/map.jinja" import swall46 with context %}

include:
  - swall46.packages_service
  - swall46.interfaces_zones
  - swall46.param_rule

