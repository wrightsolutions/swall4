# -*- coding: utf-8 -*-
# vim: ft=yaml

{% from "swall46/map.jinja" import swall46 with context %}

include:
  - swall46

conntrack:
  file.managed:
    - name: /etc/shorewall/conntrack
    - source: salt://swall46/files/conntrack.j2
    - template: jinja
    - mode: 640
    - user: root
    - group: root

params_toplevel:
  file.managed:
    - name: /etc/shorewall/params
    - source: salt://swall46/files/params.txt
    - mode: 640
    - user: root
    - group: root

params_shared:
  file.managed:
    - name: /etc/shorewall/params.shared
    - source: salt://swall46/files/params.shared.j2
    - template: jinja
    - mode: 640
    - user: root
    - group: root

params_custom:
  file.managed:
    - name: /etc/shorewall/params.custom
    - source: salt://swall46/files/params.custom.j2
    - template: jinja
    - mode: 640
    - user: root
    - group: root

policy4:
  file.managed:
    - name: /etc/shorewall/policy
    - source: salt://swall46/files/policy.j2
    - template: jinja
    - mode: 640
    - user: root
    - group: root

rules_toplevel:
  file.managed:
    - name: /etc/shorewall/rules
    - source: salt://swall46/files/rules.txt
    - mode: 640
    - user: root
    - group: root

rules_shared:
  file.managed:
    - name: /etc/shorewall/rules.shared
    - source: salt://swall46/files/rules.shared.j2
    - template: jinja
    - mode: 640
    - user: root
    - group: root

rules_custom:
  file.managed:
    - name: /etc/shorewall/rules.custom
    - source: salt://swall46/files/rules.custom.j2
    - template: jinja
    - mode: 640
    - user: root
    - group: root

