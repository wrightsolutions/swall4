# -*- coding: utf-8 -*-
# vim: ft=yaml

{% from "swall46/map.jinja" import swall46 with context %}

#include:
#  - users

/etc/shorewall:
  file.directory:
    - user: root
#    - group: root
#    - mode: 2750
    - makedirs: False

swall4pkg:
  pkg.installed:
    - pkgs:
      - shorewall
      - bc
      - perl-modules
    - refresh: True
  service.running:
    - name: shorewall
    - enable: True
    - watch:
      - file: /etc/shorewall*
#      - file: /etc/shorewall/params.shared
#      - file: /etc/shorewall/params.custom

swall4check:
 cmd.run:
    - name: '/sbin/shorewall check'
    - require:
      - pkg: swall4pkg

