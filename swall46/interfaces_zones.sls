# -*- coding: utf-8 -*-
# vim: ft=yaml

{% from "swall46/map.jinja" import swall46 with context %}

include:
  - swall46

interfaces4:
  file.managed:
    - name: /etc/shorewall/interfaces
    - source: salt://swall46/files/interfaces.j2
    - template: jinja
    - mode: 640
    - user: root
    - group: root

zones4:
  file.managed:
    - name: /etc/shorewall/zones
    - source: salt://swall46/files/zones.j2
    - template: jinja
    - mode: 640
    - user: root
    - group: root

